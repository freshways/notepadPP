# TagUI开发环境Notepad++插件配置

#### 介绍
TagUI开发环境Notepad++插件配置

通过 NotePad++ 的插件、自定义语言等配置，提升 TagUI 开发效率。
Notepad++的语法插件，需要先安装Notepad++，然后安装Snippets插件。

#### 第一步，安装 Notepad++
官方下载地址：[https://notepad-plus.en.softonic.com/download](https://notepad-plus.en.softonic.com/download)
注意：如果你是用绿色版本的，请查看是否有插件管理功能，如果没有，还是重装一下吧。

#### 第二步，安装 Snippets 插件
Snippets是一个Notepad++的代码块插件，支持自定义，用起来很方便。

Notepad++ 插件菜单  --> 插件管理  -->  搜索 Snippets，选中结果后安装。
![安装Snippets](Snippets.png)

#### 第三步，导入自定义语言
Notepad++ -->  语言菜单 -->  自定义语言 -->  自定义语言格式
导入本仓库里的： **TagUI-UserDefine.xml** 

![导入自定义语言](zdylg.png)

导入成功后，语言菜单就有了 TagUI

![导入成功后能看到这个](vtagui.png)

#### 导入Snippets插件数据库
右键 Snippets 插件导入本仓库的  **4个 .sqlite**  文件。


TagUI - 日期命令.sqlite   <------------------ TagUI自带语法

TagUI - 基础命令.sqlite   <------------------ 这是 Js 语法

TagUI - 常用命令.sqlite   <------------------ 这是 Js 语法 

TagUI - 数学命令.sqlite   <------------------ 这是 Js 语法

![右键Snippets插件导入](daoruchaj.png)


#### 最后一步，增加 “运行” 
把本仓库  **TagUI-Shortcuts.xml**  文件里的命令，增加到：

```
C:\Users\Administrator\AppData\Roaming\Notepad++\shortcuts.xml 
```
添加后，重启 Notepad++，在 “运行” 菜单就可以直接运行 .tag 了

![增加运行菜单设置](run.png)


#### 一起来丰富插件内容吧
Snippets 插件可以自定义添加很多函数，让我们一起丰富起来，让开发效率越来越高。


